<center style="background-color:lightgreen">
    <h1 style="color:black"> 
        Intro to Bash Scripting/Programming
    </h1>
</center>


## Just for starters

[what is programming language](https://en.wikipedia.org/wiki/Programming_language)
[what is scripting language?](https://en.wikipedia.org/wiki/Scripting_language)
[what is POSIX?](https://en.wikipedia.org/wiki/POSIX)
[another one](https://stackoverflow.com/questions/1780599/what-is-the-meaning-of-posix)
[what is bash shell scripting in particular ?](https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29)

Here are some examples and initial things that we need to start with in order to create shell scripts

- [Test](./0_test.sh)
- [Another Test](./1_test2.sh)

## Comments
[Comments](./4_comment.sh)

## Easier to seek error and such

[Debugging your script](./5_debug.sh)


## __Variables__

In computer programming, a variable is a storage location (identified by a memory address) paired with an associated symbolic name, which contains some known or unknown quantity of information referred to as a value. The variable name is the usual way to reference the stored value, in addition to referring to the variable itself, depending on the context. This separation of name and content allows the name to be used independently of the exact information it represents. The identifier in computer source code can be bound to a value during run time, and the value of the variable may thus change during the course of program execution.

Variables in programming may not directly correspond to the concept of variables in mathematics. The latter is abstract, having no reference to a physical object such as storage location. The value of a computing variable is not necessarily part of an equation or formula as in mathematics. Variables in computer programming are frequently given long names to make them relatively descriptive of their use, whereas variables in mathematics often have terse, one- or two-character names for brevity in transcription and manipulation. [taken from wikipedia](https://en.wikipedia.org/wiki/Variable_(computer_science))


### __Variable types__
In terms of the classifications of variables, we can classify variables based on the lifetime of them. The different types of variables are:
- **static**: A static variable is also known as global variable, it is bound to a memory cell before execution begins and remains to the same memory cell until termination.  A typical example is the static variables in Bash, Perl, C and C++.
- **stack-dynamic**: A Stack-dynamic variable is known as local variable, which is bound when the declaration statement is executed, and it is reallocated when the procedure returns. The main examples are local variables in C subprograms and Java methods
- **explicit heap-dynamic**: Explicit Heap-Dynamic variables are nameless (abstract) memory cells that are allocated and reallocated by explicit run-time instructions specified by the programmer. The main examples are dynamic objects in C++ (via new and delete) and all objects in Java
- **implicit heap-dynamic**: Implicit Heap-Dynamic variables are bound to heap storage only when they are assigned values. Allocation and release occur when values are reassigned to variables. As a result, Implicit heap-dynamic variables have the highest degree of flexibility. The main examples are some variables in JavaScript, PHP and all variables in APL

#### Why do I need to know this?
- You are studying a type of scripting language, and this is common knowledge that is needed to know.
- When acknowledging yourself to other languages, these basics will give you an advantage.


## __Variables in Bash__



### Variable scope

In Bash, the scope of user variables is generally global. That means, it does not matter whether a variable is set in the "main program" or in a "function", the variable is defined everywhere.

Compare the following equivalent code snippets
```bash
myvariable=test
echo $myvariable
```
```bash
myfunction() {
  myvariable=test
}
 
myfunction
echo $myvariable
```

#### Local variables

Bash provides ways to make a variable's scope local to a function:

- Using the `local` keyword, or
- Using `declare` (which will detect when it was called from within a function and make the variable(s) local).

```bash 
myfunc() {
local var=VALUE
 
# alternative, only when used INSIDE a function
declare var=VALUE
}
```

 The local keyword (or declaring a variable using the declare command) tags a variable to be treated completely local and separate inside the function where it was declared: 

```bash 
foo=external
 
printvalue() {
local foo=internal
 
echo $foo
}
 
 
# this will print "external"
echo $foo
 
# this will print "internal"
printvalue
 
# this will print - again - "external"
echo $foo
```

#### Environment variables

The environment space is not directly related to the topic about scope, but it's worth mentioning.

Every UNIX® process has a so-called environment. Other items, in addition to variables, are saved there, the so-called environment variables. When a child process is created (in Bash e.g. by simply executing another program, say ls to list files), the whole environment including the environment variables is copied to the new process. Reading that from the other side means: Only variables that are part of the environment are available in the child process.

A variable can be tagged to be part of the environment using the export command:
```bash
# create a new variable and set it:
# -> This is a normal shell variable, not an environment variable!
myvariable="Hello world."
 
# make the variable visible to all child processes:
# -> Make it an environment variable: "export" it
export myvariable
```


### Bash Specific benefits with variables

One core functionality of Bash is to manage parameters. A parameter is an entity that stores values and is referenced by a name, a number or a special symbol.

- parameters referenced by a name are called variables (this also applies to arrays)
- parameters referenced by a number are called positional parameters and reflect the arguments given to a shell
- parameters referenced by a special symbol are auto-set parameters that have different special meanings and uses

Parameter expansion is the procedure to get the value from the referenced entity, like expanding a variable to print its value. On expansion time you can do very nasty things with the parameter or its value. These things are described here.

If you saw some parameter expansion syntax somewhere, and need to check what it can be

Arrays can be special cases for parameter expansion, every applicable description mentions arrays later.

Looking for a specific syntax you saw, without knowing the name? 
- Simple usage
  - $PARAMETER
  - ${PARAMETER}
- Indirection
  - ${!PARAMETER}
- Case modification
  - ${PARAMETER^}
  - ${PARAMETER^^}
  - ${PARAMETER,}
  - ${PARAMETER,,}
  - ${PARAMETER~}
  - ${PARAMETER~~}
- Variable name expansion
  - ${!PREFIX*}
  - ${!PREFIX@}
- Substring removal (also for filename manipulation!)
  - ${PARAMETER#PATTERN}
  - ${PARAMETER##PATTERN}
  - ${PARAMETER%PATTERN}
  - ${PARAMETER%%PATTERN}
- Search and replace
  - ${PARAMETER/PATTERN/STRING}
  - ${PARAMETER//PATTERN/STRING}
  - ${PARAMETER/PATTERN}
  - ${PARAMETER//PATTERN}
- String length
  - ${#PARAMETER}
- Substring expansion
  - ${PARAMETER:OFFSET}
  - ${PARAMETER:OFFSET:LENGTH}
- Use a default value
  - ${PARAMETER:-WORD}
  - ${PARAMETER-WORD}
- Assign a default value
  - ${PARAMETER:=WORD}
  - ${PARAMETER=WORD}
- Use an alternate value
  - ${PARAMETER:+WORD}
  - ${PARAMETER+WORD}
- Display error if null or unset
  - ${PARAMETER:?WORD}
  - ${PARAMETER?WORD}



---
&copy; All right reserved to Alex M. Schapelle and distributed under GPLv3 License as attached to project.