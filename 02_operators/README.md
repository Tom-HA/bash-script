<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Scripting Conditions
    </h1>
</center>


## Operators

In any [programming language](https://en.wikipedia.org/wiki/Programming_language), there is set of instructions that help the management content or information.(that is also called `data`) those instructions are called [operators](https://en.wikipedia.org/wiki/Operator_(computer_programming)), and while there are many types of then, the most basic `operators` are taken from vastly known fields, such as:

- Mathematics
- Philosophy
- Electronics

thus `operators` can also be separated into types of operators as follows:

- [Operators](#operators)
    - [Operation Table](#operation-table)
      - [Arithmetic operators](#arithmetic-operators)
      - [Assignment operators](#assignment-operators)
      - [Comparison operators](#comparison-operators)
        - [NOTE: there are also bitwise operators that we do NOT dive in deeply, and just mention them in this chapter.](#note-there-are-also-bitwise-operators-that-we-do-not-dive-in-deeply-and-just-mention-them-in-this-chapter)
      - [Logical operators](#logical-operators)
      - [Membership operators](#membership-operators)
      - [Bitwise operators](#bitwise-operators)

While we speak about operators, it is important to mentation that, we mainly focus on [bourne again shell: Bash](https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29), and although all operators are available, but Bash's capability of complex calculations is some what limited, for example, calculation decimal fraction is not always possible, can be seen in this [example](./fraction_error.sh). the tools that help us to do those calculations are commands such as:
- `expr`
- `let`
- `bc`
- external shells such as `python`/`ruby`

#### Operation Table

Below is several tables of operator types, with links to specific examples.

##### Arithmetic operators

Operators | Description
--------- | -----------
++ --     |    Auto-increment and auto-decrement integer, both prefix and postfix
\* / %    |    Multiplication, division, modulus (remainder) of integer
\+ \-     |    Addition, subtraction of integer
\*\*      |    Exponent of integer

[see example](./arithmetic.sh)

---

##### Assignment operators

Operators | Description
--------- | -----------
=         | assignment of variable to value


[see example](./assignment.sh)

---

##### Comparison operators

Operators | Description
--------- | -----------
== !=     |    Equality, inequality (both evaluated left to right) of **string**
+=        |    Increment value of variable and store in the same variable (integer type)
-=        |    Reduce value of variable and store in the same variable  (integer type)
*=        |    Multiply value of variable and store in the same variable  (integer type)
/=        |    Divide value of variable and store in the same variable  (integer type)
%=        |    Modulus value of variable and store in the same variable  (integer type)
-eq       |    Equal of integer
-ne       |    Not Equal of integer
-gt       |    Greater Then of integer
-lt       |    Less Then of integer
-ge       |    Greater or Equal of integer
-le       |    Less or Equal of integer

###### NOTE: there are also bitwise operators that we do NOT dive in deeply, and just mention them in this chapter.

[see example](./comparisons.sh)

---

##### Logical operators

Operators | Description
--------- | -----------
&&        |      Logical AND 
\|\|      |      Logical OR 
\!        |      Logical NOT
 
[see example](./logical.sh)

##### Membership operators

Operators | Description
--------- | -----------


[see example](./membership.sh)

##### Bitwise operators

Operators | Description
--------- | -----------
<< >>     |     Bitwise left shift, bitwise right shift
< <= > >= |  Less than, less than or equal to, greater than, greater than or equal to
&         |      Bitwise AND
^         |      Bitwise exclusive OR
\|        |      Bitwise OR
?:        |      Inline conditional evaluation

**NOTE**

Because  let  and  ((...))  are  built  in  to  the  shell,  they  have access to variable values. It is not necessary to precede a variable’s name with a dollar sign in order to retrieve its value (doing so does work, of course).The exit status of let is confusing. It’s zero (success) for a non-zero  mathematical  result,  and  non-zero  (failure)  for  a  zero mathematical result.

[see example](./bitwise.sh)

---
&copy; All right reserved to Alex M. Schapelle and distributed under GPLv3 License as attached to project.