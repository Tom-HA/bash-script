<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Bash: Scripting Functions
    </h1>
</center>

# Functions
Defining functions

myfunc() {
    echo "hello $1"
}

--- 
# Same as above (alternate syntax)
function myfunc() {
    echo "hello $1"
}

myfunc "John"

# Returning values

myfunc() {
    local myresult='some value'
    echo $myresult
}

result="$(myfunc)"

---
# Raising errors

myfunc() {
  return 1
}

if myfunc; then
  echo "success"
else
  echo "failure"
fi

---

# Arguments

$# 	Number of arguments
$* 	All postional arguments (as a single word)
$@ 	All postitional arguments (as separate strings)
$1 	First argument
$_ 	Last argument of the previous command

> Note: $@ and $* must be quoted in order to perform as described. Otherwise, they do exactly the same thing (arguments as separate strings).



---
&copy; All right reserved to Alex M. Schapelle and distributed under GPLv3 License as attached to project.