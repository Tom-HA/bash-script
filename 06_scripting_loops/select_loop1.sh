#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################

fruits='banana melon strawberry blackberry'

PS3='Select fruit: '

select fruit in $fruits
    do
        if [ $fruit == 'Quit' ];then
            break
        fi
        echo "enjoy your $fruit"
    done