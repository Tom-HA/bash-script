#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################


count=10
a=20
b=16
 
# until loop for multiple conditions in expression
until [[ $a -lt $count || $b -lt count ]]; do
   echo "a : $a, b : $b"
   let a--
   let b--
done