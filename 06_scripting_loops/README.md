<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Bash: Scripting Loops
    </h1>
</center>


## Loops

Often we want to run the same sequence of commands, over and over again, with slight differences. This is where,Bash loop come in handy.
Bash loops are very useful. In this section of our Bash Scripting Tutorial we'll look at the different loop formats available to us as well as discuss when and why you may want to use each of them.
Loops allow us to take a series of commands and keep re-running them until a particular situation is reached. They are useful for automating repetitive tasks.
There are **4 basic** loop structures in Bash scripting which we'll look at below:
- [for](#for)
- [while](#while)
- [until](#until)
- [select](#select)
There are also a few statements which we can use to control the loops operation.


### For

The for quite simple and different from other loop types. What it does is say for each of the items in a given list, perform the given set of commands. It has the following syntax.
```sh
for var in <list>
    do
         <commands>
    done
```
The for loop will take each item in the list (in order, one after the other), assign that item as the value of the variable var, execute the commands between do and done then go back to the top, grab the next item in the list and repeat over.
The list is defined as a series of strings, separated by spaces, as can be seen [here](./for_loop1.sh), [here](./for_loop2.sh) and [here](./for_loop3.sh)

---

### For (C-Style)

The C-style for-loop is a compound command derived from the equivalent ksh88 feature, which is in turn derived from the C `for` keyword. Its purpose is to provide a convenient way to evaluate arithmetic expressions in a loop, plus initialize any required arithmetic variables. It is one of the main "loop with a counter" mechanisms available in the language.

```bash 
for (( <EXPR1> ; <EXPR2> ; <EXPR3> )) do
  <LIST>
done
```


---

### While

One of the easiest loops to work with is `while` loops. They say, while an expression is `true` (exit code of existing test is 1), keep executing these lines of code. They have the following format:
```bash
while [[ <test> ]]
    do
        <commands>
    done
```

You'll notice that `while` loop is similar to `if` statements, because of  the `test` statement that is placed between square brackets \[ \].
the difference is that `if` statement happens only once, but with `while` loop goes on forever.

---

### Until

Relatively simple, `until` loop, is exactly as it does: it executes the `<test>` and if the exit code of it was not 0 (FALSE) it executes `<commands>`. This happens again and again until `<true>` returns TRUE.

This is exactly the opposite of the while loop. 
```bash
until [[ <test> ]]
    do
        <commands>  
    done
```
As before, until loop is also some what alike `if not` statement.

here are some simple links to until loop example:
- [example1](./until_loops1.sh)
- [example2](./until_loops2.sh)

---

### Select

**This syntax is not well documented and depending on version of shell that you use, will behave differently. it is suggested not use.**

This compound command provides a kind of menu. The user is prompted with a numbered list of the given words, and is asked to input the index number of the word. If a word was selected, the variable <NAME> is set to this word, and the list <LIST> is executed.

If no in <WORDS> is given, then the positional parameters are taken as words (as if in "$@" was written).

Regardless of the functionality, the number the user entered is saved in the variable REPLY
Bash knows an alternative syntax for the select command, enclosing the loop body in `{...}` instead of `do ... done`: 

```sh
select x in 1 2 3
{
  echo $x
}
```
practical examples: [here](./select_loop1.sh) and [here](select_loops2.sh)
---

### Controlling Loops: `break` and `continue`

Usually when even you implement loops of any kind, you'd prefer to run in sequential manner.However there will be cases where we'll need to intervene and alter their running slightly. There are two statements we may issue to do this: 
- [break](#break)
- [continue](#continue)


#### Break

The break statement tells Bash to leave the loop straight away. It may be that there is a normal situation that should cause the loop to end but there are also exceptional situations in which it should end as well. For instance, maybe we are copying files but if the free disk space gets below a certain level we should stop copying. Check [this example](break.sh) to see it in action.

#### Continue

The continue statement tells Bash to stop running through this iteration of the loop and begin the next iteration. Sometimes there are circumstances that stop us from going any further. For instance, maybe we are using the loop to process a series of files but if we happen upon a file which we don't have the read permission for we should not try to process it.
Check [this example](continue.sh) to see it in action.

---

### Nested Loops

Nested loop are a very simple concept: loop inside of a loop. e.g.

```bash
for (( i = 1; i <= 5; i++ ))      ### Outer for loop ###
do

    for (( j = 1 ; j <= 5; j++ )) ### Inner for loop ###
    do
          echo -n "$i "
    done

  echo "" #### print the new line ###
done

```
For each value of i the inner loop is cycled through 5 times, with the variable j taking values from 1 to 5. The inner for loop terminates when the value of j exceeds 5, and the outer loop terminates when the value of i exceeds 5. 
---


&copy; All right reserved to Alex M. Schapelle and distributed under GPLv3 License as attached to project.