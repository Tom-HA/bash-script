#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################


count=10
i=20
 
# until loop with single condition
until [ $i -lt $count ]; do
   echo "$i"
   let i--
done